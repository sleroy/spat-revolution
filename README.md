> - SPAT Revolution Ultimate:  1995 $ / [Premium member](https://forum.ircam.fr/subscribe/?subscription=login) Price: 997,50 $
> - 22.09 Perpetual License Upgrade (Requires an active current full SPAT Revolution Ultimate License): 299 $ / [Premium member](https://forum.ircam.fr/subscribe/?subscription=login) Price: 149,50 $
> - Spat Revolution Ultimate (Add-on Option): 499 $ / [Premium member](https://forum.ircam.fr/subscribe/?subscription=login) Price: 249,50 $
> - SPAT Revolution Essential: 399 $ / [Premium member](https://forum.ircam.fr/subscribe/?subscription=login) Price: 199,50 $ - [Ask for the voucher](https://forum.ircam.fr/contact/)
> - Distributed by [Flux::](https://shop.flux.audio/en_US/products/spat-revolution)
> - [Technical Support](https://support.flux.audio/portal/sign_in) / [Customized technical assistance](https://shop.flux.audio/en_US/products/spat-revolution)
> - Get you trial [Try](https://shop.flux.audio/en_US/page/trial-request-information)
> - Software available by downloading the [Flux:: Center ](https://www.flux.audio/download/)
> - iLok.com user account is required (USB Smart Key is not required)


![Flux::](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/logos-flux%2Bircam-noir.png)

A Leap In Power and Performance - The v20.12 update contains a comprehensive list of enhancements and includes a Multi-Core Parallel Computation Algorithm, Hardening with all latest operating systems (OS) and DAWs, with macOS Catalina, Big Sur, and Apple notarization official support.

## SPAT Revolution 22.09 ##

![Spat Revolution 22.09](https://forum.ircam.fr/media/uploads/Softwares/Spat%20Revolution/spat_revolution_22.09.jpeg)

**The SPAT Revolution 2022.09 update** presents a new version of FLUX:: Immersive’s comprehensive spatialization tool SPAT Revolution, offering new workflow features including a new Snapshot management system, Delay/Latency compensation for software and hardware inputs, New Item list page for simpler management, and updated speaker editor to ease managing loudspeaker arrangement planning. 

License owners that want to upgrade to 22.09 simply need to log in to their FLUX:: account to access their product upgrade offer in the FLUX:: online store. The upgrade is available for license owners within the realms of the FLUX:: EDU program, application can be done [here](https://www.flux.audio/education/). 

[Full release notes](https://doc.flux.audio/#/en_US/spat_revolution_doc/Appendix_D_release_notes) / [Test Drive WFS](https://www.flux.audio/spat-revolution-ultimate-wfs-test-drive/) / [More information](https://www.flux.audio/spat-revolution-22-02/)


## Main Features ##

![Flux::](https://forum.ircam.fr/media/uploads/Softwares/Spat%20Revolution/preview-full-img_8646.jpg)

Two license options to choose from

- **SPAT Revolution Essential**
Single virtual room environment ; deliver channel-base, binaural and ambisonic up to 3rd order ; channel-based predefined and custom setups up to 12ch. (including Dolby Atmos) ; up to 32 source audio channels ; Ambisonic 1st order source ; virtualization of speaker setup (binaural monitoring) ; HRTF Library of 12 models ; OSC integration ; predefined OSC transformation presets ; simple setup management  ; up to 96Khz    

- **SPAT Revolution Ultimate**
Multi virtual room environments ; deliver simultaneously for channel-base, binaural and ambisonic up to 7th order ; channel-based  predefined and custom setups up to 64ch ; unlimited audio channels ; Ambisonic up to 7th order (HOA) ; multi environment virtualization (binaural monitoring) ; complete HRTF Listen library and SOFA import ; OSC and RTTrPM tracking integration ; custom OSC transformation  ; complete flexible module setup configuration ; up to 384Khz ; Nebula Spatial Spectrogram

## Advanced Features ##
### Speaker Configuration Management ###
- Import speaker arrangements from simulation software of Adamson Blueprint AV, CODA Audio, Nexo NS-1, d&b audiotechnik, EASE, or simply, from Excel
- Speaker arrangement panning tips and validation guidelines
- Per speaker channel mute option in the virtual room
- Modify your speaker arrangements with offset, mirror, scale and many more transformations
- Add speakers in batches with various uniform distribution types
- Insert background images of various size to your room arrangement with scaling and offset options

### Objects, Snapshots and Transformation ###
Deploy new ways to manage your object-based mix with the snapshot system. Change an audio mix scene with snapshot recall with interpolation time done manually, via OSC or with touch remote control (ex: Lemur). To facilitate transformations like moving sources, source transform feature, with an interpolation time, allows you to manipulate sources/objects positions
- Virtual mixing room environment with source and speaker name tag 
- Snapshot system giving the ability to create static images and recall with interpolation time
- Source transformation for source/object action with interpolation time
- iOS remote templates (Lemur) for touch interface to controlling sources and snapshots (with Multi-touch template)

### Powerful OSC interpreter and Real-time tracking system ### 
- 8 OSC Connections
- BlackTrax Real Time Tracking RTTrPM Protocol input (Ultimate only)
- Sources and Listener positions tracking in 6 DOF
- IRCAM ADMix, Spat and Meyer SpaceMap Go OSC grammar on input.
- ADM-OSC support on input and output
- OSC Output options for Auto-Bundle and Touch/Release messages
- OSC presets for SPAT plug-ins, Lemur controllers, Avid S6L, Digico, SSL Live and ADM-OSC
- OSC transformation allowing for value rescaling, offsetting, converting and much more (Pre-defined presets with Essential license)
- Use control OSC messages (index -1) for active selection.

![Flux::](https://forum.ircam.fr/media/uploads/Softwares/Spat%20Revolution/preview-full-img_8643.jpg)

### Encoding and Decoding with Transcoding Modules ###
SPAT Revolution objects stream type can span from many types of microphones, arrays, HOA captures, and from any type of pre-produced stems. It can render content in HOA up to 7th order. Supporting a vast range of stream formats, transcoder modules allow modification of the channel count of the stream passing through it, depending on the format transfer being requested. For example, transcoding from Ambisonic B-Format into a Channel Based 3D Cube involves a four-channel Ambisonic stream getting transcoded into an eight-channel stream grouped and treated as a specific speaker configuration

- **A-Format microphone support**: Sennheiser Ambeo, Soundfield, Core Sound, DPA Oktava, Røde
- **Steam type Input to Output transcoding**: A-Format to B-Format ; A/B-Format to Channel Based ; A/B-Format to HOA ; B-Format to UHJ ; Binaural to Transaural ; Channel Based (C.B) to Channel Based; Channel splitter aggregator ; HOA to Channel-based ; HOA to HOA for conversion of normalization, channel-arrangement, 2D/3D ; HOA presets for AmbiX, A & B-Format ; Mid/Side (MS) to Channel Based ; UHJ to B-Format
- **Decoding Methods**: Projection ; regularized Pseudo-Inverse ; energy preserving ; AllRad ; improved AllRAD
- **Decoding Type**: Basic ; InPhase ; MaxRe ; BasicMaxRe ; MaxReInPhase ; InPhaseMaxRe type
- **Normalization options**: N2D/N3D ; SN2D/SN3D ; FuMa ; MaxN
- **Sorting options**: ACN ; SID ; FMH

### Multi-Virtual Room (Ultimate Only) ###
In SPAT Revolution the source objects are spatialized inside virtual rooms with spatial positioning, panning, reverberation and output format taken into consideration when calculating the output rendering.
In order to encompass various different workflows or render different deliverables, multiple rooms can be created and used simultaneously, in parallel, with individual independent output formats.
Summing this up is the ability to binaurally monitor (virtualize) a scene from a channel based output, giving an impression of how the mix might sound diffused by a particular speaker arrangement (including the space between speakers and gain characteristics belonging to selected panning types).

### Unique Nebula Spatial Spectrogram (Ultimate Only) ### 
Nebula is a technology adapted from FLUX:: Analyzer System, simulating how sound sources localize their  sound over different speaker setups, providing a unique representation of the sound-field, in terms of spectral content and localization, in real-time inside the 3D virtual room display.

## Bring a sense of space and depth to your mix with the next generation audio mixing concepts ##
A revolutionary object & perceptual immersive mixing tool redefining the way of mixing where you intuitively position objects in spaces and let the acoustic signature of the room build the desired depth
Bring your various audio sources as objects moving into a virtual space in a output format agnostic way while allowing you to create acoustic space (reverberation) with localized reflections.
It simply means spaciousness to your mix from multiple sound source formats supported.

![Spat Revolution](https://forum.ircam.fr/media/uploads/Softwares/Spat%20Revolution/macbook-pro-768x465-spat.png)

## Render for various deliverables ##
Audio is literally living a paradigm shift, moving beyond stereo to various spatial audio techniques.  At the core of SPAT Revolution is a desire not to impose on panning methods and spatialization techniques rather to offer a wide range of possibilities,  including panning techniques for working with audiences in wide areas not confined to traditional sweet spots for live and installation setups.
Being for improving the frontal resolution for concert diffusion, to immerse the audience with surround audio, to render to any channel-based or scene-based formats, to deliver spatial audio over headphones, SPAT Revolution simply opens up to the most advanced techniques and brings them to your fingertips.

## Designed to integrate into any workflow ##
Rapidly deploy and manage your object-based mix session using a simple setup wizard using your physical or virtual audio interface of choice as your hardware I/O or vastly expand your DAW’s capabilities using SPAT plugin suites (AAX, AU, VST) and audio pipe technology for software audio routing and parameters automation
Add remote integration using control devices, show control application, trajectory software and digital mixing consoles over the network, through the Open Sound Control (OSC) protocol.
Take advantage of [ReaVolution](https://www.flux.audio/project/reavolution-for-spat-revolution/), a Reaper package for SPAT Revolution facilitating the setup, integration, and workflow for immersive audio creation and production.

### SPAT Revolution REMOTE ###
REMOTE offers multiple control sections, each for some specific function:
- Main Page: 2D pad and faders for chosen parameters
- Dual-Source XY: Two (2) 2D Pad for controlling two sources simultaneously
- Multi-Source XY: Controlling Eight (8) sources of choice 
- Mixer: Most used mixer functions presented on banks of 8 faders
- Source: All parameters of SPAT Revolution sources
- Reverb: Control for the reverb parameters of up to 6 rooms.
- Room: Output control for up to 6 rooms
- Snapshot: Next, Previous, Manual recall for snapshots with recall time option.

## Specifications ##
- **Availability**: SPAT Revolution standalone application Windows and MacOS  ; SPAT Plugins (Send, Return and Room), AU, AAX, AAX VENUE and VST2
- **Processing**: 
32/64-bits internal floating point processing. 
Sampling rate up to 384 kHz, Block size buffer starting at 16 blocks (hardware dependant): Up to 96kHZ (Essential) ; Up to 384kHZ (Ultimate)
Unlimited number of Input and Output (Hardware and audio interface dependant): 32 audio sources (Essential)

- **Software Licence Requirements**: 
 - Essential: Ilok account required ; Machine authorization is supported ; iLok USB Smart Key support (From 2nd gen) ; iLok Cloud support, SPAT Essential license ; Simultaneous authorizations (Single activation)
- Ultimate: Ilok account required ; Machine authorization is supported ; iLok USB Smart Key support (From 2nd gen) ; iLok Cloud support, SPAT Essential license & SPAT Ultimate license (Included in SPAT Ultimate bundle) ; Simultaneous authorizations

- **OS Compatibility**:
- Windows – 7 SP1, 8.1 and 10, all in 64 bits only
- macOS (Intel) – All versions from 10.13 (64bit only), macOs Big Sur compliant.

- **Hardware Recommendations and Requirements**: 
*Single Computer (Creative Station with DAW and Spat Revolution)*
Single computer systems can provide a great option for portability but come with the importance of having a performing computer. As Spat Revolution comes with a 3D graphic engine where audio objects are manipulated, GPU resources are required. 

* Base system / Portable Computer
- Processing: Intel Core i9-9900K, i7-9700K or equivalent. Intel 8th generation or greater processor. (Minimum 6 cores – 8 prefered), 8 MB Cache.
- System Memory: 16 minimum GB DDR4 (32 preferred when integrating with DAW and Spat Revolution on the same machine with local audio path – LAP) 
- Chipset: High-quality / Professional mainboard such as Intel® Z370 Chipset
- Graphic: GeForce GTX 1060 – 4GB GDDR5 Graphic Memory or greater (Graphic card fully supporting OpenGL 2.0 is required. USB displays are not supported)
- Audio Interfaces: Mac OS X: Core Audio compatible interface or virtual sound card, Windows: ASIO compatible interface or virtual sound card. Preferred interface: USB Audio interface 
- OS: Mac OS High Sierra, Mojave or Catalina. Windows Pro 10 64-bit.

*Hardware for Live Productions (Dedicated Spat Revolution Computer)*
Recommended audio interfaces: Dante PCIe interfaces from Yamaha, Focusrite, or any Dante PCIe high-density card. ; RME Digiface AVB, USB 3 Audio interface ;RME Madi HDSPe MADI FX or MADIface XT.

Recommended Spat workstation system: Intel® Xeon® W family W-2200 or W-3200 Series CPU (preferred), E-22xx, or equivalent. Higher core speed. Minimum 8 Cores, 12 MB Cache ; Chipset: Professional workstation chipset C422, C621, or equivalent.  ; System Memory: 32 – 64 GB. A system with ECC support preferred. ; Graphic: Professional NVIDIA Quadro P4000 or equivalent – 8GB GDDR5 Graphic Memory (Graphic card with full support for OpenGL 2.0)  ; Network: Dual network interface (NIC) – Intel I210-T1 or equivalent  ; OS drive (Operating System): NVMe Internal SSD ; Audio Projects Hard Drive: No specifics. For redundant systems, only certified RAID systems should be used. ; OS: Mac OS High Sierra, Mojave, or Catalina. Windows Pro 10 64-bit. Real-Time optimized OS.

Mac OS X: OpenGL 2.0 required – Mac Pro 1.1 & Mac Pro 2.1 are not supported.
Windows: If your computer has an ATi or NVidia graphics card, please assure the latest graphic drivers from the ATi or NVidia website are installed.

[![Facebook](https://forum.ircam.fr/media/uploads/Softwares/Spat%20Revolution/dcabf181-f446-4bf1-9970-f137c9345be4-24562-0000131c7932758a.png.jpeg)](https://facebook.com/groups/fluximmersive.usergroup)

## [Ableton Live Tools for SPAT Revolution](https://www.flux.audio/project/ableton-live-tools-for-spat-revolution/) ##

A complete workflow set up for Immersive Audio creation and production using a set of custom-built Live Devices created to integrate Ableton Live with SPAT Revolution


>**Documentation**
>
> - [Online Use Guide](https://doc.flux.audio/#/en_US/spat_revolution_doc/A_User_Guide) / [Essential Quick Start Guide](https://forum.ircam.fr/media/uploads/Softwares/Spat%20Revolution/spat_revolution_essential_quick_start_guide.pdf) /
[Resources](https://www.flux.audio/knowledge-base/category/support-documents-files/spat-revolution-resources/) / [Support & Knowledge Base](https://www.flux.audio/knowledge-base/category/spat-revolution/)
>
>**Tutorials**
> - [How to set up SPAT Révolution with ReaVolution](https://youtu.be/JeSRPa_Gf-0) - English and French
>
> - [How to set up SPAT Revolution with Ableton Live](https://youtu.be/UWQg7p7HWJM) English and French
>
> - [How to set up NUENDO with Spat Revolution by Felix Niklasson](https://www.flux.audio/2020/05/01/how-to-set-up-your-daw-with-spat-revolution/)
>
> -	[Introduction to Spat Revolution – Online Training Session](https://www.youtube.com/watch?v=x3U3scY2_E8&feature=emb_logo)
>
>**[Technical Articles](https://www.flux.audio/category/spat-revolution-tech/)** / **[YouTube Channel](https://www.youtube.com/fluximmersive)** / **[News & Press](https://www.flux.audio/category/news/)** / **[User Stories](https://www.flux.audio/category/user-stories/)** / **[News & Press](https://www.flux.audio/category/news/)** / **[Release Notes](https://doc.flux.audio/#/en_US/spat_revolution_doc/Appendix_D_release_notes)**
>
>**Article**
> - [Introducing ReaVolution – Integrating SPAT Revolution with Reaper, March 2021]( https://www.flux.audio/2021/03/04/introducing-reavolution-integrating-spat-revolution-with-reaper/)

>
